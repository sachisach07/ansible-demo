 **Step 1: Create an EC2 instance**

**Step 2: Installing python and boto**
1. apt update
2. apt install pip
4. apt install boto boto3

**Step 3: Installing Ansible on Ubuntu**
1. sudo apt update
2. sudo apt install software-properties-common
3. sudo add-apt-repository --yes --update ppa:ansible/ansible
4. sudo apt install ansible

After installing the ansible go to /etc/ansible/hosts (update the present IP address in hosts file)

**Step 4: Write a yaml file for start an instance with a public IP address **
Link for yml file --> https://gitlab.com/sachisach07/ansible-demo/-/blob/main/launch_instance.yml?ref_type=heads

After writing the yaml file lets run the file using command called --> ansible-playbook launch_instance.yml





